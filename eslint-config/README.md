# Resolution ESLint Config

Adds our internal ESLint config, which integrates prettier, SonarJS and supports Typescript.

### Prerequisites

You need to already have a `tsconfig.json` file that fits your needs.

### How to use

First, install the config and the plugins:

```bash
npm add --save-dev @resolution/eslint-config @resolution/prettier-config eslint prettier typescript
# OR
yarn add -D @resolution/eslint-config @resolution/prettier-config eslint prettier typescript
```

Then, add the following section to your `package.json`:

```json
{
 "prettier": "@resolution/prettier-config"
}
```

and create a `.eslintrc.js`:

```js
module.exports = {
    "extends": "@resolution/eslint-config/profile/react",
    "parserOptions": { "tsconfigRootDir": __dirname }
}
```

### Profiles

We offer a couple of profiles you can use at the end of the `extends` string:

* `base`: Anything Typescript
* `react`: Typescript + React
