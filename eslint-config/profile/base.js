require('@rushstack/eslint-patch/modern-module-resolution');

module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        sourceType: 'module',
        ecmaVersion: 2019,
    },
    plugins: [
        'sonarjs',
        'unused-imports',
        "simple-import-sort",
    ],
    extends: [
        'plugin:@typescript-eslint/recommended', // Uses the recommended rules from @typescript-eslint/eslint-plugin
        'plugin:sonarjs/recommended', // Uses sonarjs rules
        'plugin:prettier/recommended', // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    rules: {
        'sonarjs/cognitive-complexity': 'off',
        'sonarjs/no-identical-functions': 'off',
        'sonarjs/max-switch-cases': 'off',
        'sonarjs/no-nested-switch': 'off',
        curly: ['warn', 'all'],
        eqeqeq: ['error', 'always'],
        'no-void': 'error',
        "no-unused-vars": "off",
        "@typescript-eslint/no-unused-vars": "off",
        "unused-imports/no-unused-imports": "error",
        "unused-imports/no-unused-vars": ["warn", {
            "vars": "all",
            "varsIgnorePattern": "^_",
            "args": "after-used",
            "argsIgnorePattern": "^_",
            'ignoreRestSiblings': true,
            'destructuredArrayIgnorePattern': '^_',
        }],
        '@typescript-eslint/no-misused-promises': ['error', {}],
        '@typescript-eslint/no-floating-promises': ['error', {}],
        '@typescript-eslint/strict-boolean-expressions': ['error', {
            allowString: true,
            allowNumber: true,
            allowNullableObject: true,
            allowNullableBoolean: true,
            allowNullableString: true,
            allowNullableNumber: true,
            allowNullableEnum: true,
        }],
        '@typescript-eslint/no-unsafe-declaration-merging': 'error',
        '@typescript-eslint/consistent-type-imports': ['error', {
            fixStyle: 'inline-type-imports'
        }],
        '@typescript-eslint/no-import-type-side-effects': 'error',
        '@typescript-eslint/require-array-sort-compare': 'error',
        'simple-import-sort/imports': 'error',
        'no-nested-ternary': 'error',
        'no-console': ['warn', {
            'allow': ['debug', 'info', 'warn', 'error']
        }]
    },
    ignorePatterns: [
        'types.d.ts',
        '/src/jira.swagger.types.ts',
        '/src/confluence.swagger.types.ts',
        'babel.config.js',
        'jest.config.ts',
        'next.config.js',
        '.eslintrc.js',
        'dist',
        'lib'
    ],
};
