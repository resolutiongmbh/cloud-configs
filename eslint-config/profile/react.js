const base = require('./base');
module.exports = {
    ...base,
    plugins: [
        'react-hooks',
        ...base.plugins,
    ],
    extends: [
        'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
        'plugin:react-hooks/recommended',
        'plugin:react/jsx-runtime',
        ...base.extends,
    ],
    rules: {
        ...base.rules,
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'error',
        'react/prop-types': 'off',
        'react/jsx-uses-react': 'off',
        'react/react-in-jsx-scope': 'off'
    },
    settings: {
        react: {
            version: 'detect'
        }
    }
};
