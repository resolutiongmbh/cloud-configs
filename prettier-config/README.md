# Resolution Prettier Config

Best used with our ESLint config at @resolution/eslint-config

Adds our internal prettier config.

### How to use

First, install the config and prettier:

```bash
npm add --save-dev @resolution/prettier-config prettier
# OR
yarn add -D @resolution/prettier-config prettier
```

Then, add the following section to your `package.json`:

```json
{
  "prettier": "@resolution/prettier-config"
}
```

Alternatively, you can of course also use the separate config file approach.
